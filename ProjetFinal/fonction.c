#include <conio.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include "variables.h"

void vertical(); void horizontal();

void attendre(float temps)
{
	clock_t arrivee = clock() + (temps*CLOCKS_PER_SEC);
	while(clock() < arrivee);
}
void aff(char c[], int coul, int coul2)
{
	int i, j;
	j = strlen(c);
	_textcolor(coul2);
	_textbackground(coul);
	for(i=0; i<j; i++)
	{
		printf("%c", c[i]);
		attendre(0.08);
	}
	_textbackground(7);
	_textcolor(0);
}
void remplissage(int x1, int x2, int y1, int y2, int couleur)
{
	int i,  j;
	_textcolor(couleur);
	for(i = x1+1;  i<=x2;  i++)
	{
		for(j=0; j<=(y2-y1); j++)
		{
			_gotoxy(i, y1+j);
			printf("%c", 219);
		}
	}
	_textcolor(0);
}

void cadre1(int x1, int x2, int y1, int y2)
{
	int i;
	for(i=x1; i<=x2; i++)
	{
		_gotoxy(i, y1);
		printf("%c", 205);
		_gotoxy(i, y2);
		printf("%c", 205);
	}

	for(i=y1; i<=y2; i++)
	{
		_gotoxy(x1, i);
		printf("%c", 186);
		_gotoxy(x2, i);
		printf("%c", 186);
	}
	_gotoxy(x1, y1);
	printf("%c", 201);
	_gotoxy(x1, y2);
	printf("%c", 200);
	_gotoxy(x2, y1);
	printf("%c", 187);
	_gotoxy(x2, y2);
	printf("%c", 188);
}

void cadre2(int x1, int x2, int y1, int y2)
{
	int i;
	for(i=x1+2; i<=x2-2; i++)
	{
		_gotoxy(i,y1);
		printf("%c", 196);
		_gotoxy(i,y2);
		printf("%c",196);
	}
	for(i=y1+2; i<=y2-2; i++)
	{
		_gotoxy(x1,i);
		printf("%c", 179);
		_gotoxy(x2,i);
		printf("%c", 179);
	}
		_gotoxy(x1+1,y1+1);
		printf("%c", 217);//218
		_gotoxy(x2-1,y1+1);
		printf("%c", 192);//191
		_gotoxy(x1+1,y2-1);
		printf("%c", 191);//192
		_gotoxy(x2-1,y2-1);
		printf("%c", 218);//217

		_gotoxy(x1+1,y1);
		printf("%c", 218);//218
		_gotoxy(x1,y1+1);
		printf("%c", 218);
		_gotoxy(x2-1,y1);
		printf("%c", 191);//191
		_gotoxy(x2,y1+1);
		printf("%c", 191);
		_gotoxy(x1+1,y2);
		printf("%c", 192);//192
		_gotoxy(x1,y2-1);
		printf("%c", 192);
		_gotoxy(x2-1,y2);
		printf("%c", 217);//217
		_gotoxy(x2,y2-1);
		printf("%c", 217);
}

void billet(int x1, int x2, int y1, int y2)
{
	cadre(x1, x2, y1, y2);
	cadre2(x1+2, x2-2, y1+1, y2-1);
}

void affundescore(int x, int y, int taille)
{
	int i;
	for(i=x;  i < x + taille;  i++)
	{
		_gotoxy(i, y);
		printf("%c", 238);
	}
}

void affcadre(int x, int y, char s[500])
{
	int i = x, l=8, j=0;
	l = strlen(s);
	_textcolor(0);
	while(j <= l-1)
	{
		cadre3(i, i+2, y, y+1);
		//_sleep(1);
		attendre(0.08);
		_gotoxy(i+1, y);
		i += 3;
		j++;
	}
	j = 0; i = x;
	_textcolor(1);
	while(j <= l-1)
	{
		_gotoxy(i+1, y);
		printf("%c", s[j]);
		//_sleep(1);
		attendre(0.2);
		i += 3;
		j++;
	}
	_textcolor(0);

}

void cercleA(int r, int a, int b)
{
	int x, y;
	for(y = -r-1; y< r+1; y++)
	{
		printf("\n");
		_gotoxy(a+x, b+y);
		for(x = -r; x<r+1; x++)
		{
			if(abs((x*x) + (y*y) - (r*r)) <= (r/2))
			{
				_textbackground(0);
				_textcolor(14);
				printf(" *");
				_textcolor(15);
				_textbackground(3);
				attendre(0.03);
			}
			else
			{
				//_gotoxy(a+x, b+y);
				printf("  ");
			}
		}

	}

}

void cercle()
{
	int w = 1, v = 0;
	cercleA(8, 35, 20);
	_gotoxy(57, 16); _textcolor(10);printf("EBOOKING");_textcolor(11);
	_textbackground(0);
	_gotoxy(46, 20);
	printf("  Ceci est une r%calisation de:  ", 130);
	_gotoxy(52, 21);
	printf(" POUGOUE & SOCGNA ");
	_textbackground(3);
	_textcolor(15); _gotoxy(54, 25); printf("Version: 1.0.0");
	_gotoxy(2, 45);
}

void tableau(int x, int y, int nl, int nc)
{
	int i,j, l = 18;
	cadre(x, x+l*nc, y, y+2*nl);
	for(i = 0; i < nl; i++)
	{
		horizontal(y+2*i, x+1, l*nc+x-1);
	}
	for(i = 0; i < nc; i++)
	{
		vertical(x+l*i, y+1, y-1+2*nl);
	}
	for(i = 1; i < nl; i++)
	{
		_gotoxy(x, y + 2*(i));printf("%c",195);
		_gotoxy(x+l*nc, y + 2*(i));printf("%c",180);
		for(j = 1; j < nc; j++)
		{
			_gotoxy(x+l*j, y+2*i);printf("%c",197);
    			_gotoxy(x+l*j,y);printf("%c",194);
    			_gotoxy(x+l*j,y+2*nl);printf("%c",193);
		}
	}
}

void listePassager(char matriculeB[10])
{
	int i, j;
	for(i = 0; i<cptB; i++)
	{
		if(strcmp(matriculeB, bus[i][0].busb.matriculeB) == 0)
		{
			if(bus[i][2].clientRb.confirm == 1)
			{
				for(j = 0; j<cptR; j++)
				{
					if(j % 2 == 0)
					{
						_textbackground(2);
						remplissage(38, 96, 14+3*j-1, 14+3*j+1, 2);
					}
					if(j% 2 != 0)
					{
						_textbackground(15);
						remplissage(38, 96, 14+3*j-1, 14+3*j+1, 15);
					}
						_gotoxy(43, 14+3*j); printf("%d", j+1);
						_gotoxy(50, 14+3*j); printf("%s", R[j].nom);
						_gotoxy(71, 14+3*j); printf("%s",  R[j].prenom);
						_gotoxy(92, 14+3*j); printf("%d",  R[j].age);
				}
			}
		}
	}
}
