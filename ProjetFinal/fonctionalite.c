#include "variables.h"

void menu()
{
		int l;
		_gotoxy(100, 10);
		printf("*** Que voulez vous faire? ***");
		_gotoxy(104, 12);
		printf("1- Modifier");
		_gotoxy(104, 13);
		printf("2- Revenir au Menu");
		_gotoxy(93, 15);
			printf("Faites votre choix: ");
			l = strlen("Faites votre choix: ");
			//ecrire(l, 1, 7, 1, 93, 6, 5, 22);
			affundescore(l+93,16, 5);
}

void accueil(int color)
{
	int ch, l;
	system("cls");
		presentation();
		remplissage(1, 14, 7, 9, color);
		_textbackground(color);_textcolor(14);
		_gotoxy(2,8);printf("   Accueil");_textcolor(0);
		_textbackground (8);
		remplissage(15, 89, 7, 37, color);
		affA(color);

}

void voyage(int color)
{
int ch, l;
	system("cls");
	do
	{
		presentation();
		remplissage(1, 14, 11, 13, color);
	_textbackground(color);_textcolor(14);
	_gotoxy(2,12);printf("    Voyage");_textcolor(0);
	_textbackground (7);
	remplissage(15, 89, 7, 37, color);
		affV(color);
		modifV(color);
		printf("Faites votre choix: ");
		l = strlen("Faites votre choix: ");
		affundescore(93+l,19,5);
		_gotoxy(95+l, 18);
		scanf("%d", &ch);
		switch(ch)
		{
			case 1:
				reservation(color);
				break;
			case 2:
				confirmation(color);
				break;
			case 3:
				annulation(color);
				break;
			case 4:
				modification(color);
				break;
			case 5:
				break;
			default:
				_gotoxy(93, 20);
				_textcolor(12);
				printf("Choix invalide");
				_textcolor(15);
				_gotoxy(2, 39);
				break;
		}

	}while(ch != 5);

}

void location(int color)
{
int ch, l;
	system("cls");
	do
	{
		presentation();
		remplissage(1, 14, 15, 17, color);
	_textbackground(color);_textcolor(14);
	_gotoxy(2,16);printf("  Location");_textcolor(0);
	_textbackground (7);
	remplissage(15, 89, 7, 37, color);
		affL(color);
		modifV(color);
		printf("Faites votre choix: ");
		l = strlen("Faites votre choix: ");
		affundescore(93+l,19,5);
		_gotoxy(95+l, 18);
		scanf("%d", &ch);
		switch(ch)
		{
			case 1:
				reservationL(color);
				break;
			case 2:
				confirmationL(color);
				break;
			case 3:
				annulationL(color);
				break;
			case 4:
				modificationL(color);
				break;
			case 5:
				break;
			default:
				_gotoxy(93, 20);
				_textcolor(12);
				printf("Choix invalide");
				_textcolor(15);
				_gotoxy(2, 39);
				system("pause");
				break;
		}

	}while(ch != 5);
}

void collis(int color)
{
	int ch, l;
	system("cls");
	do
	{
		presentation();
		remplissage(1, 14, 19, 21, color);
	_textbackground(color);_textcolor(14);
	_gotoxy(2,20);printf("    Collis");_textcolor(0);
	_textbackground (7);
	remplissage(15, 89, 7, 37, color);
		affC(color);
		l = strlen("Faites votre choix: ");
		affundescore(93+l, 21, 5);
		_gotoxy(95+l, 20);
		scanf("%d", &ch);
		switch(ch)
		{
			case 1:
				modifC(color);
				break;

			case 2:
				consultation(color);
				break;
			case 3:
				break;
			default:
				_gotoxy(93, 25);
				_textcolor(12);
				printf("Choix invalide");
				_textcolor(15);
				_gotoxy(2, 39);
				system("pause");
				break;
		}

			_gotoxy(1,39);
			system("pause");
	}while(ch != 3);

}

void equipe(int color)
{
int ch, l, i;
	system("cls");
	do
	{
		presentation();
		remplissage(1, 14, 23, 25, color);
	_textbackground(color);_textcolor(14);
	_gotoxy(2,24);printf("  5.Equipes");_textcolor(0);
	_textbackground (7);
	remplissage(15, 89, 7, 37, color);
		affE(color);
		l = strlen("Faites votre choix: ");
		affundescore(93+l, 21, 5);
		_gotoxy(95+l, 20);
		scanf("%d", &ch);
		switch(ch)
		{
			case 1:
				choixE(color);
				break;
			case 2:
				remplissage(15, 89, 7, 37, color);
				_textbackground(color);_gotoxy(49, 8);_textcolor(14);printf("=== EQUIPE ===");_textcolor(15);
				tableau(16, 14, cptEE+1, 4);
				_gotoxy(25, 15); printf("EQUIPE", 248);_gotoxy(40, 15); printf("CONDUCTEUR");_gotoxy(60, 15); printf("HOTESSE");
				_gotoxy(72, 15); printf("PERIODE");
				for(i = 0; i < cptEE; i++)
				{
					_gotoxy(25, 17+2*i); printf("%s", EE[i].nom);
					_gotoxy(38, 17+2*i); printf("%s", EE[i].matriculeC);
					_gotoxy(60, 17+2*i); printf("%s", EE[i].matriculeH);
					_gotoxy(79, 17+2*i); printf("%s", EE[i].periode);
				}
				break;
		}
	}while(ch != 3);

}

void divers(int color)
{
int ch, l;
	system("cls");
	do
	{
		presentation();
		remplissage(1, 14, 27, 29, color);
	_textbackground(color);_textcolor(14);
	_gotoxy(2,28);printf("  6.Divers");_textcolor(0);
	_textbackground (7);
	remplissage(15, 89, 7, 37, color);
		affD(color);
		l = strlen("Faites votre choix: ");
		affundescore(93+l, 25, 5);
		_gotoxy(95+l, 24);
		scanf("%d", &ch);
		switch(ch)
		{
			case 1:
				buget(color);
				break;
			case 2:
				VoyCours(color);
				break;
			case 3:
				 gererBus(color);
				break;
			case 4:
				note(color);
				break;
			case 5:
				break;
		}
	}while(ch != 5);

}

void apropos(int color)
{
int ch, l;
	system("cls");
	do
	{
		presentation();
		remplissage(1, 14, 31, 33, color);
	_textbackground(color);_textcolor(14);
	_gotoxy(2,32);printf("  7.A propos");_textcolor(0);
	_textbackground (7);
	remplissage(15, 89, 7, 37, color);
		affP(color);
		modifP();
		smsP(color);
		l = strlen("a l'accueil: ");
		_gotoxy(98+l, 14);
		scanf("%d", &ch);
		_gotoxy(1,39);
		switch(ch)
		{
			case 1:
				break;
			default:
				_gotoxy(93, 20);
				_textcolor(12);
				printf("Choix invalide");
				_textcolor(15);
				_gotoxy(2, 39);
				system("pause");
				break;
		}
	}while(ch != 1);
}
